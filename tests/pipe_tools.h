#ifndef PIPE_TOOLS_H
#define PIPE_TOOLS_H

#include <common/protocol.h>
#include <common/errors.h>

auto CreatePipe(const std::string& pipeName, bool isAsync)
{
    HANDLE hPipe = CreateFileA(
                pipeName.c_str(),
                GENERIC_READ | GENERIC_WRITE,
                0,
                nullptr,
                OPEN_EXISTING,
                isAsync ? FILE_FLAG_OVERLAPPED : 0,
                nullptr
                );

    return hPipe;
}


bool ReadFromStream(HANDLE hPipe, size_t expectedSize, std::ostream& os)
{
    const DWORD tempBuffSize = 0x4000;
    char buff[tempBuffSize];

    DWORD bytesToRead = static_cast<DWORD>(expectedSize);

    while (bytesToRead != 0)
    {
        DWORD readBytes = 0;
        if (!ReadFile(hPipe, buff, std::min(bytesToRead, tempBuffSize), &readBytes, nullptr))
            return false;

        os.write(buff, readBytes);
        bytesToRead -= readBytes;
    }

    return true;
}

template<typename T>
Result<T> ReadFromPipe(HANDLE hPipe)
{
    auto msgHdrOs = MakeRWStream();
    auto msgBodyOs = MakeRWStream();

    if (!ReadFromStream(hPipe, messages::MessageHeader::SerializedSize, msgHdrOs))
        return nonstd::make_unexpected(eUnexpected);

    messages::MessageHeader msgHeader;
    {
        auto result = Deserialize<messages::MessageHeader>(msgHdrOs);
        if (!result)
            return result.get_unexpected();
        msgHeader = result.value();
    }

    if (!ReadFromStream(hPipe, msgHeader.length, msgBodyOs))
        return nonstd::make_unexpected(eUnexpected);

    return Deserialize<T>(msgBodyOs);
}


#endif // PIPE_TOOLS_H
