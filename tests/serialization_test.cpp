#include <gtest/gtest.h>

#include <common/protocol.h>


TEST(MessageHeaderSerialization, ReadWriteMessageHeaderSucceeded)
{
    using namespace messages;

    MessageHeader srcHdr;
    srcHdr.type = MT_InvokeReq;
    srcHdr.length = 100500;

    auto stream = MakeRWStream();

    MessageHeader::Serialize(srcHdr, stream);
    auto result = MessageHeader::Deserialize(stream);
    EXPECT_FALSE(!result);
    MessageHeader dstHdr = result.value();

    EXPECT_EQ((int)srcHdr.type, (int)dstHdr.type);
    EXPECT_EQ(srcHdr.length, dstHdr.length);
}

TEST(MessageHeaderSerialization, ReadMessageHeaderFromEmptyBuffFailed)
{
    using namespace messages;

    auto stream = MakeRStream(std::string());

    auto result = MessageHeader::Deserialize(stream);
    EXPECT_TRUE(!result);
    EXPECT_EQ(eDataTooSmall, (int)result.error());
}
