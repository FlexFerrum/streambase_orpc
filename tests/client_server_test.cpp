#include "server_test_fixture.h"
#include "test_remote_object.h"

#include <named_pipe_client.h>
#include <sync_connection.h>
#include <async_connection.h>

class ClientServerTest : public ServerTestFixture
{
protected:
    void InitObjectManager()
    {
        m_objectManager.RegisterType<TestDataStruct>("TestDataStruct",
            {
                {"intAttr", reflect::Attribute(&TestDataStruct::intAttr)},
                {"doubleAttr", reflect::Attribute(&TestDataStruct::doubleAttr)},
                {"strAttr", reflect::Attribute(&TestDataStruct::strAttr)},
            },
            {}
        );

        m_objectManager.RegisterType<TestClass>("TestClass",
            {
                {"intAttr", reflect::Attribute(&TestClass::GetIntAttr)},
                {"doubleAttr", reflect::Attribute(&TestClass::GetDoubleAttr)},
                {"strAttr", reflect::Attribute(&TestClass::GetStrAttr)},
            },
            {
                {"TestMethod1", reflect::Method(&TestClass::TestMethod1)},
                {"TestMethod2", reflect::Method(&TestClass::TestMethod2)},
                {"TestMethod3", reflect::Method(&TestClass::TestMethod3)},
                {"TestMethod4", reflect::Method(&TestClass::TestMethod4)},
                {"TestMethod5", reflect::Method(&TestClass::TestMethod5)},
                {"TestMethod6", reflect::Method(&TestClass::TestMethod6)},
                {"SetIntAttr", reflect::Method(&TestClass::SetIntAttr)},
                {"SetDoubleAttr", reflect::Method(&TestClass::SetDoubleAttr)},
                {"SetStrAttr", reflect::Method(&TestClass::SetStringAttr)},
            }
        );
    }
};

TEST_F(ClientServerTest, SimpleSyncConnection)
{
    bool clientConnected(false);

    std::mutex guardMutex;
    std::condition_variable cv;

    m_server->SetOnClientAccepted([&clientConnected, &guardMutex, &cv] {
        ExecAndNotify(guardMutex, cv, [&clientConnected]{clientConnected = true;});
    });

    NamedPipeClient client;

    client.Start();

    auto connection = client.ConnectSync(m_pipeName);
    EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&clientConnected]{return clientConnected;}));

    connection.reset();

    client.Stop();
}

TEST_F(ClientServerTest, SimpleSyncObjectInteraction)
{
    InitObjectManager();

    NamedPipeClient client;

    client.Start();

    auto connection = client.ConnectSync(m_pipeName);
    EXPECT_FALSE(!connection);

    auto remoteObj1 = connection->CreateRemoteObject("TestDataStruct");
    EXPECT_TRUE(remoteObj1->IsAlive());
    TestDataStruct ethalon1;
    EXPECT_EQ(ethalon1.intAttr, remoteObj1->GetAttribute<int>("intAttr").value());
    EXPECT_DOUBLE_EQ(ethalon1.doubleAttr, remoteObj1->GetAttribute<double>("doubleAttr").value());
    EXPECT_EQ(ethalon1.strAttr, remoteObj1->GetAttribute<std::string>("strAttr").value());

    auto remoteObj2 = connection->CreateRemoteObject("TestClass");
    EXPECT_EQ(30, remoteObj2->CallMethod<int>("TestMethod2", 10, 20).value());
    EXPECT_DOUBLE_EQ(30.123, remoteObj2->CallMethod<double>("TestMethod4", 10.123, 20).value());
    EXPECT_EQ(std::string("Hello World!"), remoteObj2->CallMethod<std::string>("TestMethod3", std::string("Hello "), std::string("World!")).value());
    EXPECT_EQ(std::string("1111111111"), remoteObj2->CallMethod<std::string>("TestMethod5", std::string("1"), 10).value());

    TestClass ethalon2;
    EXPECT_EQ(ethalon2.GetIntAttr(), remoteObj2->GetAttribute<int>("intAttr").value());
    EXPECT_DOUBLE_EQ(ethalon2.GetDoubleAttr(), remoteObj2->GetAttribute<double>("doubleAttr").value());
    EXPECT_EQ(ethalon2.GetStrAttr(), remoteObj2->GetAttribute<std::string>("strAttr").value());
    EXPECT_FALSE(!remoteObj2->CallMethod<void>("SetIntAttr", ethalon1.intAttr + 10));
    EXPECT_FALSE(!remoteObj2->CallMethod<void>("SetDoubleAttr", ethalon1.doubleAttr + 10));
    EXPECT_FALSE(!remoteObj2->CallMethod<void>("SetStrAttr", ethalon1.strAttr + "10"));
    EXPECT_EQ(ethalon2.GetIntAttr() + 10, remoteObj2->GetAttribute<int>("intAttr").value());
    EXPECT_DOUBLE_EQ(ethalon2.GetDoubleAttr() + 10, remoteObj2->GetAttribute<double>("doubleAttr").value());
    EXPECT_EQ(ethalon2.GetStrAttr() + "10", remoteObj2->GetAttribute<std::string>("strAttr").value());

    EXPECT_EQ((int)eAttributeNotFound, (int)remoteObj2->GetAttribute<int>("int1Attr").error());
    EXPECT_EQ((int)eMethodNotFound, (int)remoteObj2->CallMethod<int>("foo").error());
    EXPECT_EQ((int)eTypeNotFound, (int)connection->CreateRemoteObject("TestClass1").error());

    connection.reset();

    EXPECT_FALSE(remoteObj1->IsAlive());
    EXPECT_FALSE(remoteObj2->IsAlive());
    EXPECT_EQ((int)eDisconnected, (int)remoteObj2->CallMethod<int>("foo").error());

    client.Stop();
}

TEST_F(ClientServerTest, SimpleAsyncConnection)
{
    bool clientConnected(false);

    std::mutex guardMutex;
    std::condition_variable cv;

    m_server->SetOnClientAccepted([&clientConnected, &guardMutex, &cv] {
        ExecAndNotify(guardMutex, cv, [&clientConnected]{clientConnected = true;});
    });

    NamedPipeClient client;

    client.Start();

    auto connectionFuture = client.ConnectAsync(m_pipeName);

    EXPECT_TRUE(connectionFuture.valid());
    ASSERT_EQ(std::future_status::ready, connectionFuture.wait_for(std::chrono::milliseconds(1000)));
    auto connection = connectionFuture.get();
    EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&clientConnected]{return clientConnected;}));

    connection.reset();

    client.Stop();
}

TEST_F(ClientServerTest, SimpleAsyncObjectInteraction)
{
    InitObjectManager();

    bool clientConnected(false);

    std::mutex guardMutex;
    std::condition_variable cv;

    m_server->SetOnClientAccepted([&clientConnected, &guardMutex, &cv] {
        ExecAndNotify(guardMutex, cv, [&clientConnected]{clientConnected = true;});
    });

    NamedPipeClient client;

    client.Start();

    auto connectionF = client.ConnectAsync(m_pipeName);
    const auto defTimeout = std::chrono::milliseconds(500);
    EXPECT_EQ(std::future_status::ready, connectionF.wait_for(defTimeout));
    auto connection = connectionF.get();

    auto remoteObjF = connection->CreateRemoteObject("TestClass");
    EXPECT_EQ(std::future_status::ready, remoteObjF.wait_for(defTimeout));
    auto remoteObj = remoteObjF.get().value();

    auto tm5F = remoteObj.CallMethod<std::string>("TestMethod5", std::string("1"), 10);
    auto tm4F = remoteObj.CallMethod<double>("TestMethod4", 10.123, 20);
    auto tm6_1F = remoteObj.CallMethod<int>("TestMethod6", 100);
    auto tm1F = remoteObj.CallMethod<int>("TestMethod2", 10, 20);
    auto tm6_2F = remoteObj.CallMethod<int>("TestMethod6", 200);
    auto tm3F = remoteObj.CallMethod<std::string>("TestMethod3", std::string("Hello "), std::string("World!"));
    auto tm6_3F = remoteObj.CallMethod<int>("TestMethod6", 400);
    auto invalidRemoteObjF = connection->CreateRemoteObject("TestClass123");
    auto tm7F = remoteObj.CallMethod<int>("TestMethod7");
    auto intAttrF = remoteObj.GetAttribute<int>("intAttr");
    auto uintAttrF = remoteObj.GetAttribute<int>("uintAttr");

    EXPECT_EQ(std::future_status::ready, tm1F.wait_for(defTimeout));
    EXPECT_EQ(30, tm1F.get().value());

    EXPECT_EQ(std::future_status::ready, tm4F.wait_for(defTimeout));
    EXPECT_DOUBLE_EQ(30.123, tm4F.get().value());

    EXPECT_EQ(std::future_status::ready, tm3F.wait_for(defTimeout));
    EXPECT_EQ(std::string("Hello World!"), tm3F.get().value());

    EXPECT_EQ(std::future_status::ready, tm5F.wait_for(defTimeout));
    EXPECT_EQ(std::string("1111111111"), tm5F.get().value());

    EXPECT_EQ(std::future_status::ready, tm6_1F.wait_for(defTimeout));
    EXPECT_EQ(100, tm6_1F.get().value());

    EXPECT_EQ(std::future_status::ready, tm6_2F.wait_for(defTimeout));
    EXPECT_EQ(200, tm6_2F.get().value());

    EXPECT_EQ(std::future_status::ready, tm6_3F.wait_for(defTimeout));
    EXPECT_EQ(400, tm6_3F.get().value());

    EXPECT_EQ(std::future_status::ready, intAttrF.wait_for(defTimeout));
    EXPECT_EQ(TestClass().GetIntAttr(), intAttrF.get().value());

    EXPECT_EQ(std::future_status::ready, tm7F.wait_for(defTimeout));
    EXPECT_EQ((int)eMethodNotFound, (int)tm7F.get().error());

    EXPECT_EQ(std::future_status::ready, uintAttrF.wait_for(defTimeout));
    EXPECT_EQ((int)eAttributeNotFound, (int)uintAttrF.get().error());

    EXPECT_EQ(std::future_status::ready, invalidRemoteObjF.wait_for(defTimeout));
    EXPECT_EQ((int)eTypeNotFound, (int)invalidRemoteObjF.get().error());
    connection.reset();

    EXPECT_FALSE(remoteObj.IsAlive());

    client.Stop();
}
