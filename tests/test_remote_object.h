#ifndef TEST_REMOTE_OBJECT_H
#define TEST_REMOTE_OBJECT_H

#include <string>

struct TestDataStruct
{
    int intAttr = 100500;
    double doubleAttr = 100.5;
    std::string strAttr = "Hello World!";
};

class TestClass
{
public:
    TestClass() = default;

    void TestMethod1()
    {
        std::cout << "TestClass::TestMethod1 called\n";
    }
    int TestMethod2(int a, int b)
    {
        std::cout << "TestClass::TestMethod2 called\n";
        return a + b;
    }
    std::string TestMethod3(const std::string& a, const std::string& b)
    {
        std::cout << "TestClass::TestMethod3 called\n";
        return a + b;
    }
    double TestMethod4(double a, double b)
    {
        std::cout << "TestClass::TestMethod4 called\n";
        return a + b;
    }
    std::string TestMethod5(const std::string& str, int repeat)
    {
        std::string result;
        for (int n = 0; n < repeat; ++ n)
            result += str;

        return result;
    }
    int TestMethod6(int delay)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));

        return delay;
    }

    void SetIntAttr(int val) {intAttr = val;}
    void SetDoubleAttr(double val) {doubleAttr = val;}
    void SetStringAttr(const std::string& val) {strAttr = val;}

    auto GetIntAttr() const {return intAttr;}
    auto GetDoubleAttr() const {return doubleAttr;}
    auto GetStrAttr() const {return strAttr;}

private:
    int intAttr = 100500;
    double doubleAttr = 100.5;
    std::string strAttr = "Hello World!";
};

#endif // TEST_REMOTE_OBJECT_H
