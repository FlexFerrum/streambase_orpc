#ifndef SERVER_TEST_FIXTURE_H
#define SERVER_TEST_FIXTURE_H

#include <gtest/gtest.h>
#include <named_pipe_server.h>
#include <remote_object_manager.h>

#include <condition_variable>
#include <mutex>

template<typename Fn>
void ExecAndNotify(std::mutex& m, std::condition_variable& cv, Fn&& fn)
{
    {
        std::unique_lock<std::mutex> l(m);
        fn();
    }
    cv.notify_all();
}

template<typename D, typename Fn>
bool WaitNotify(std::mutex& m, std::condition_variable& cv, D&& d, Fn&& fn)
{
    std::unique_lock<std::mutex> l(m);
    return cv.wait_for(l, d, std::forward<Fn>(fn));
}

class BasicServerTestFixture : public testing::Test
{
public:
    void SetUp() override
    {
        m_server = std::make_unique<NamedPipeServer>(m_pipeName, &m_objectManager);
    }
    void TearDown() override
    {
        m_server.reset();
    }

protected:
    const std::string m_pipeName = "TestORPCPipe";
    std::unique_ptr<NamedPipeServer> m_server;
    RemoteObjectManager m_objectManager;
};

class ServerTestFixture : public BasicServerTestFixture
{
public:
    void SetUp() override
    {
        BasicServerTestFixture::SetUp();

        bool serverStarted = false;
        std::mutex guardMutex;
        std::condition_variable cv;
        m_server->SetOnPipeCreated([&, firstTime = true](bool isCreated) mutable {
            if (!firstTime)
                return;
            firstTime = false;
            ExecAndNotify(guardMutex, cv, [&serverStarted, isCreated]{serverStarted = isCreated;});
        });

        m_server->Start();

        EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&serverStarted]{return serverStarted;}));

    }
    void TearDown() override
    {
        m_server->Stop();
        BasicServerTestFixture::TearDown();
    }
};

#endif // SERVER_TEST_FIXTURE_H
