#include <gtest/gtest.h>

#include "server_test_fixture.h"
#include "pipe_tools.h"

#include <common/protocol.h>
#include <common/errors.h>

#include <condition_variable>
#include <mutex>

#include <windows.h>

using ScopedLock = std::unique_lock<std::mutex>;

TEST_F(BasicServerTestFixture, StartStopTest)
{
    bool serverStarted = false;
    bool serverStopped = false;

    std::mutex guardMutex;
    std::condition_variable cv;

    m_server->SetOnServerStarted([&serverStarted, &guardMutex, &cv]() mutable {
        ExecAndNotify(guardMutex, cv, [&serverStarted]{serverStarted = true;});
    });
    m_server->SetOnServerStopped([&serverStopped, &guardMutex, &cv]() mutable {
        serverStopped = true;
    });

    m_server->Start();
    EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&serverStarted]{return serverStarted;}));
    EXPECT_TRUE(serverStarted);

    m_server->Stop();
    EXPECT_TRUE(serverStopped);
}

TEST_F(BasicServerTestFixture, SingleConnectionTest)
{
    enum CreationStatus
    {
        NotCalled,
        PipeNotCreated,
        PipeCreated
    };
    CreationStatus pipeCreated(NotCalled);
    std::atomic_bool clientConnected(false);

    std::mutex guardMutex;
    std::condition_variable cv;

    m_server->SetOnPipeCreated([&pipeCreated, &guardMutex, &cv](bool isCreated) {
        ExecAndNotify(guardMutex, cv, [&pipeCreated, isCreated]{pipeCreated = isCreated ? PipeCreated : PipeNotCreated;});
    });
    m_server->SetOnClientAccepted([&clientConnected, &guardMutex, &cv] {
        ExecAndNotify(guardMutex, cv, [&clientConnected]{clientConnected = true;});
    });

    m_server->Start();
    EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&pipeCreated]{return pipeCreated != NotCalled;}));
    EXPECT_EQ((int)PipeCreated, pipeCreated);

    auto pipeName = "\\\\.\\pipe\\" + m_pipeName;

    auto hPipe = CreatePipe(pipeName, false);

    EXPECT_FALSE(hPipe == INVALID_HANDLE_VALUE);
    if (hPipe != INVALID_HANDLE_VALUE)
    {
        EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&clientConnected]{return clientConnected.load();}));
        CloseHandle(hPipe);
    }
    else
    {
        DWORD err = GetLastError();
        std::cout << "Can't create pipe " << pipeName << ". Error: " << err << std::endl;
    }

    m_server->Stop();
}


TEST_F(BasicServerTestFixture, MultipleConnectionsTest)
{
    bool pipeCreated = false;
    int clientsConnected = -1;

    std::mutex guardMutex;
    std::condition_variable cv;

    m_server->SetOnPipeCreated([&pipeCreated, &guardMutex, &cv](bool isCreated) {
        ExecAndNotify(guardMutex, cv, [&pipeCreated, isCreated]{pipeCreated = isCreated;});
    });
    m_server->SetOnClientAccepted([&clientsConnected, &guardMutex, &cv] {
        ExecAndNotify(guardMutex, cv, [&clientsConnected]{clientsConnected = clientsConnected == -1 ? 1 : clientsConnected + 1;});
    });

    m_server->Start();

    auto pipeName = "\\\\.\\pipe\\" + m_pipeName;
    const int numClientPipes = 10;
    for (int n = 0; n < numClientPipes; ++ n)
    {
        EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&pipeCreated]
        {
            bool result = pipeCreated;
            if (pipeCreated)
                pipeCreated = false;
            return result;
        }));

        auto hPipe = CreatePipe(pipeName, false);

        EXPECT_FALSE(hPipe == INVALID_HANDLE_VALUE);
        if (hPipe != INVALID_HANDLE_VALUE)
        {
            EXPECT_TRUE(WaitNotify(guardMutex, cv, std::chrono::milliseconds(500), [&n, &clientsConnected]{return clientsConnected == n + 1;}));
            CloseHandle(hPipe);
        }
        else
        {
            DWORD err = GetLastError();
            std::cout << "Can't create pipe " << pipeName << ". Error: " << err << std::endl;
        }
    }

    WaitNotify(guardMutex, cv, std::chrono::milliseconds(1000), [&clientsConnected, numClientPipes]{return clientsConnected == numClientPipes;});

    m_server->Stop();
}

TEST_F(ServerTestFixture, ConnectionStartTest)
{
    auto pipeName = "\\\\.\\pipe\\" + m_pipeName;
    auto hPipe = CreatePipe(pipeName, false);

    EXPECT_FALSE(hPipe == INVALID_HANDLE_VALUE);
    auto os = MakeWStream();
    size_t sizeToWrite = PrepareMessage(messages::ConnectionRequest{1, 0, false}, os);

    auto buff = os.str();
    DWORD writtenMsg = 0;
    EXPECT_TRUE(WriteFile(hPipe, buff.data(), sizeToWrite, &writtenMsg, nullptr));
    EXPECT_EQ(sizeToWrite, static_cast<size_t>(writtenMsg));

    auto readRes = ReadFromPipe<messages::ConnectionResponse>(hPipe);
    EXPECT_FALSE(!readRes);
    if (readRes)
    {
        const messages::ConnectionResponse& resp = readRes.value();
        EXPECT_EQ(1, (int)resp.versionHi);
        EXPECT_EQ(0, (int)resp.versionLo);
    }

    CloseHandle(hPipe);
}
