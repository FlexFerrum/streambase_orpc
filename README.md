# StreamBase ORPC

[![Language](https://img.shields.io/badge/language-C++-blue.svg)](https://isocpp.org/)
[![Standard](https://img.shields.io/badge/c%2B%2B-14-blue.svg)](https://en.wikipedia.org/wiki/C%2B%2B#Standardization)
[![Build status](https://ci.appveyor.com/api/projects/status/v9wcbgnl9dfup9x5/branch/master?svg=true)](https://ci.appveyor.com/project/flexferrum/streambase-orpc/branch/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e99bc944459441fa87bbbaa4d7b3babe)](https://www.codacy.com/app/flexferrum/streambase_orpc?utm_source=FlexFerrum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=FlexFerrum/streambase_orpc&amp;utm_campaign=Badge_Grade)

**StreamBase ORPC** is a simple namedpipe-based windows client-server libraries set which allow to create and manage objects on the server-side remotely. Library set consists of client-side library (`/src/client/lib`), server-side library (`/src/server/lib`), common tools (`/src/common`), tests and samples.

## Key assumptions

Initial requirements for the tool was not completely clear that's why some assumptions was made during the implementation process:

1. Sync/async client-server interaction. From the prospective of RPC/ORPC 'async interaction' term can be interpreted in the two different ways: asynchronous transport and asynchronous application level interface. First interpretation assumes ability of non-blocking low-level call from client to server i.e. call without waiting of successful data transfer from the client to server. The second one assumes ability of definition of the asynchronous application-level interfaces between client and server i.e. call ability to define callback application-level interfaces, invoke callback interface methods from the server etc. Requirements say nothing specific about application-level async interfaces. They say about ability of making sync/async connections from client to server which refers to async transport level, but not async application level.

2. Because of this is a test assignment with strict requirement of 'windows named pipes' usage and some time limits, implementation contains nothing special which allows to replace windows named pipes (as a transport) with another kind of transport (e.g. sockets, UNIX domain sockets, shared memory etc.).

## Key design principles

Here is the description of some design principles which were used during the implementation.

### Transport part

The transport level of `StreamBase ORCP` built with usage of `boost::asio` library. This library contains perfectly-implemented asyic IO services and tools and allows to build both sync and async network services quite fast with smooth integration with C++. Also it allows to use windows named pipes for the data transfer purpose. I considered `gRPC` library as an option for the transport layer, but it currently doesn't support windows named pipes.
In this implementation server always do async read from the pipe and make (or not) actual call of the invoked method on the same thread depending on the connection type (sync or async). In case of async connection method call is postponed via asio io_service.

### Protocol part

The protocol level of `StreamBase ORPC` built with usage of `boost::serialization`. This library is simple-to-use and allows to serialize/deserialize big range of C++ (and boost-specific) types and doesn't require special tools for serialization/deserialization procedures generation. In case of production-ready solution I'd prefer something like Google Protobuf which has lower footprint.

### Application-level interface

Chosen approach assumes that client side can perform arbitrary call to the server with lazy (delayed) type checking. I. e. only server-side services know about exact objects, their attributes and methods, actual signatures and types. Client has no way to check correctness of call before the call. This approach is simpler than usage of full-featured proxy/stub interface and perfectly fit into requirements, but more error-prone.

### Error handling

For reporting and transferring errors from the server side to the client library 'expected-lite' (written by Martin Moene) was chosen. This library implements of the upcoming C++ standard proposal of `expected` template type. This template allows to return either actual return value or specified error type as a method result without exceptions usage. So it's possible to implement error handling as a part of the method/function main flow without splitting it on (possible nested) try/catch blocks or make actual result value as an additional 'output' parameter of the method/function.

### Explicit `Start`/`Stop` methods

Initialization and deinitialization of asio io_service logic was implemented as the dedicated `Start` and `Stop` methods both for client and server classes. This decision allows to postpone actual start (and stop) of the working threads (and other logic) to the most suitable point, which (in general) may not coincide with the corresponding object creation/destruction points.

### Waiting for async result

`std::promise`/`std::future` was chosen as the implementation of waiting logic in case of async client-server interaction. This is default (and standard) way to transfer result (or an error) from the asynchronously started job to the starter. `std::future` supports `wait` mechanics so client can limit the time of waiting the result. Or wait infinitely.

## Build and run

The repository and submodules contains everything for successful project built (without any additional parameters). So, the building of project is quite simple:

1. Clone the repo with `git clone https://FlexFerrum@bitbucket.org/FlexFerrum/streambase_orpc.git streambase_orpc`
2. Change the working directory to the cloned `streambase_orpc`
3. Clone the submodules `git submodule update --init`
4. Make the build directory `mkdir -p .build` and go into it: `cd .build`
5. Run the CMake `cmake .. -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Release`
6. Build the project with `cmake --build . --target all`
7. Run tests: `ctest -C Release -V`
8. Run sample client and server

### Build pre-requirements:

1. Visual Studio 2015 (I can't guarantee successful build with 2017 because didn't check this build environment)
2. CMake 3.5 or higher
3. Access to GitHub

### Sample client and server apps

As it required, the sample client and server applications are provided. They are both console-based apps with simple command line interface.
#### Server sample (orpc_server)
`orpc_server` app is a sample of server-side application. It runs the named pipe server and provides object available for creation. As the only one command-line parameter it takes name of the pipe to listen on. Name should be without the prefix:

`orpc_server SampleOrpcServer`

Server can be stopped by `Enter` key hit.

#### Client sample (orpc_client)

`orpc_client` app is a simple of the client-side application. It connects to the specified named pipe and perform sync or async interaction with the server. The sample takes two command line params: name of the pipe (without prefix) and mode of interaction (`sync` or `async`):

`orpc_client SampleOrpcServer sync` - for synchronous interaction with the server
`orpc_client SampleOrpcServer async` - for the asynchronous interaction with the server

Client creates the object on the server side and do some operations (methods call, attributes retrieval). 

## Brief description of the implemenation

Root directory of the repository contains the following subdirs:

1. `cmake` with useful cmake scripts
2. `src` with all sources of the proejct
3. `tests` with gtest-based autotests
4. `thirdparty` with all necessary third-party stuff such as `boost`, `gtest` etc.

The `src` directory contains the following three parts of the project (as subdirs):


1.`common` with all common stuff such as common includes, RPC protocol description and implementation.
2.`client` with implementation of the client-side library (in the `lib` subdirectory) and sample client
3. `server` with implementation of the server-side library (in the `lib` subdirectory) and sample server

Implementation client-side and server-side logic as libraries was chosen in order to simplify the unit testing.

### Server side

Server-side logic is implemented with the following classes:

1. `NamedPipeServer` (/src/server/lib/named_pipe_server.h) The one of the two root server-side classes. It takes the pipe name as a constructor parameter and starts/stops asio io_service workers in the `Start` and `Stop` logic. The main purpose of this class is holds common for all client connections logic (i. e. `io_service`, `worker`, reference to the types registry and so one).
2. `RemoteObjectManager` (/src/server/lib/named_pipe_server.h) The second root server-side class which holds the registry of the types accessible to the client. It allows to register the type (with methods and attributes) via `RegisterType` method and creates the instance of the given type via `CreateObject` method. Pointer to the instance of this class is passed as the second param of `NamedPipeServer` constructor.
3. `ConnectionProcessor` (/src/server/lib/client_processor.h) 'Inner' library class which serves the logic of interaction with the single client (sync or async). It asynchronously reads requests from the pipe, creates the objects, calls its methods/gets attributes, holds the list of the created objects.
4. `ObjectStub` (/src/server/lib/object_stub.h) Provides abstraction layer between particular registered type and `ClientProcessor`. It allows to call the specified type's method via `CallMethod`method or retrieve the specified attribute via `GetAttribute`.

### Client side

Client-side logic is implemented with the following classes:

1. `NamedPipeClient` (/src/client/lib/named_pipe_client.h) The only one root client-side class. As the `NamedPipeServer` class it holds the common for all connections logic and objects such as `io_service`. It allows to create sync and async connections to the specified server pipes via `ConnectSync` and `ConnectAsync` respectively.
2. `SyncConnection` (/src/client/lib/sync_connection.h) Implements sync-connection-specific interface and logic of the interaction. It allows to create the object remotely via `CreateRemoteObject`, to release it via `ReleaseRemoteObject` and invokes the server-side method via `InvokeSync`. All client-server interactions, performed by this class, are blocked till the response from the server is sent.
3. `AsyncConnection` (/src/client/lib/async_connection.h) Do the same job as `SyncConnection` but asynchronously. Instead of actual results of the methods call it returns the `std::future` class instances on which user can wait.
4. `SyncObjectPtr`/`AsyncObjectPtr` (/src/client/lib/object_ptr.h) Represent the pointer to the server-side object concept (for sync and async interaction respectively). The `pointer` concept because of the client-side object handle can be invalidated due to connection close or object destruction. These classes allow to remotely call methods (via `CallMethod`) of the registered types and retrieve their attributes (via `GetAttribute`). They take the name of the method (or attribute), additional params and send this data to the remote side. `SyncObjectPtr` waits for the invocation result, but `AsyncObjectPtr` returns immediately with the `std::future` object.

### Common lib

Common lib provides the common logic (and types) both for the server and client sides. It contains:

1. Errors description (/src/common/common/errors.h)
2. Common types (/src/common/common/types.h)
3. RPC protocol description (/src/common/common/protocol.h) and it's implementation (/src/common/protocol.cpp)
4. boost::variant-based value type, which is used as a data container (/src/common/common/param_value.h)
5. Generic logic of the C++ class method invocation (/src/common/common/invoke_impl.h)