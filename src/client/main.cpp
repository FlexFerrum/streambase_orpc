#include "named_pipe_client.h"
#include "sync_connection.h"
#include "async_connection.h"

#include <iostream>
#include <string>

using namespace std::string_literals;

void DoSyncDemo(std::string pipeName, NamedPipeClient& client)
{
    auto connectResult = client.ConnectSync(std::move(pipeName));
    if (!connectResult)
    {
        std::cout << "Can't connect to " << pipeName << "\n";
        return;
    }

    auto remoteObj = connectResult->CreateRemoteObject("ServerObject");
    if (!remoteObj)
    {
        std::cout << "Can't create 'ServerObject' on server. Exit.\n";
        return;
    }

    std::cout << "Remote object name: " << *remoteObj->GetAttribute<std::string>("objectName") << "\n";
    std::cout << "Echo service result: " << *remoteObj->CallMethod<std::string>("Echo", "Hello to server from client!"s) << "\n";
    std::cout << "Current console write mode: " << remoteObj->GetAttribute<int>("consoleWriteMode").value() << "\n";
    remoteObj->CallMethod<void>("WriteToConsole", "Hello World 1"s);

    remoteObj->CallMethod<void>("SetConsoleWriteMode", 1);
    std::cout << "Current console write mode: " << remoteObj->GetAttribute<int>("consoleWriteMode").value() << "\n";
    remoteObj->CallMethod<void>("WriteToConsole", "Hello World 2"s);

    remoteObj->CallMethod<void>("SetConsoleWriteMode", 2);
    std::cout << "Current console write mode: " << remoteObj->GetAttribute<int>("consoleWriteMode").value() << "\n";
    remoteObj->CallMethod<void>("WriteToConsole", "Hello World 3"s);
}

void DoAsyncDemo(std::string pipeName, NamedPipeClient& client)
{
    auto connectFuture = client.ConnectAsync(std::move(pipeName));
    if (!connectFuture.valid())
    {
        std::cout << "Can't connect to " << pipeName << "\n";
        return;
    }

    auto connection = connectFuture.get();
    if (!connection)
    {
        std::cout << "Can't connect to " << pipeName << "\n";
        return;
    }

    auto remoteObjectFuture = connection->CreateRemoteObject("ServerObject");
    auto remoteObj = remoteObjectFuture.get();
    if (!remoteObj)
    {
        std::cout << "Can't create 'ServerObject' on server. Exit.\n";
        return;
    }

    auto echoFuture = remoteObj->CallMethod<std::string>("Echo", "Hello world from the async client!"s);
    std::cout << "Echo service result: " << echoFuture.get().value() << "\n";

    auto f1 = remoteObj->CallMethod<void>("WriteToConsole", "Console line 1"s);
    auto f2 = remoteObj->CallMethod<void>("WriteToConsole", "Console line 2"s);
    auto f3 = remoteObj->CallMethod<void>("WriteToConsole", "Console line 3"s);
}

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        std::cerr << "Usage: orpc_client <pipe_name> <sync|async>\n";
        return -1;
    }

    NamedPipeClient client;

    client.Start();
    std::string mode(argv[2]);
    if (mode == "sync")
        DoSyncDemo(argv[1], client);
    else if (mode == "async")
        DoAsyncDemo(argv[1], client);
    else
        std::cout << "Invalid mode specifier. Should be 'sync' or 'async', got: '" << mode << ".\n";

    client.Stop();
}
