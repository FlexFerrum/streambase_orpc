#ifndef NAMED_PIPE_CLIENT_H
#define NAMED_PIPE_CLIENT_H

#include <boost/asio/io_service.hpp>
#include <boost/asio/windows/stream_handle.hpp>
#include <boost/system/error_code.hpp>

#include <thread>
#include <functional>
#include <memory>
#include <future>

class SyncConnection;
class AsyncConnection;
using AsyncConnectionPtr = std::shared_ptr<AsyncConnection>;

class NamedPipeClient
{
public:
    NamedPipeClient();

    void Start();
    void Stop();

    void SetOnClientStarted(std::function<void ()> fn)
    {
        m_onClientStarted = std::move(fn);
    }

    void SetOnClientStopped(std::function<void ()> fn)
    {
        m_onClientStopped = std::move(fn);
    }


    std::shared_ptr<SyncConnection> ConnectSync(std::string pipeName);
    std::future<AsyncConnectionPtr> ConnectAsync(std::string pipeName);

private:
    HANDLE CreatePipe(std::string pipeName, bool isAsync);

private:

    std::shared_ptr<boost::asio::io_service> m_ioService;
    std::shared_ptr<boost::asio::io_service::work>  m_work;
    std::unique_ptr<std::thread> m_serviceThread;

    std::function<void ()> m_onClientStarted;
    std::function<void ()> m_onClientStopped;
};

#endif // NAMED_PIPE_CLIENT_H
