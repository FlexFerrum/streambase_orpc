#include "named_pipe_client.h"
#include "sync_connection.h"
#include "async_connection.h"

#include <boost/asio/windows/stream_handle.hpp>
#include <boost/system/error_code.hpp>

#include <iostream>

NamedPipeClient::NamedPipeClient()
{

}

void NamedPipeClient::Start()
{
    m_ioService = std::make_shared<boost::asio::io_service>();
    m_work = std::make_shared<boost::asio::io_service::work>(*m_ioService);
    m_serviceThread.reset(new std::thread([this]
    {
        if (m_onClientStarted)
            m_onClientStarted();

        m_ioService->run();
        if (m_onClientStopped)
            m_onClientStopped();
    }));
}

void NamedPipeClient::Stop()
{
    if (!m_ioService)
        return;

    m_ioService->stop();
    m_work.reset();
    m_serviceThread->join();
    m_serviceThread.reset();
    m_ioService.reset();
}

std::shared_ptr<SyncConnection> NamedPipeClient::ConnectSync(std::string pipeName)
{
    std::shared_ptr<SyncConnection> result;

    auto hPipe = CreatePipe(std::move(pipeName), false);
    if (hPipe == INVALID_HANDLE_VALUE)
        return result;

    auto connection = std::make_shared<SyncConnection>(hPipe);
    auto connectResult = connection->Connect();

    if (!connectResult)
        return result;

    return connection;
}

std::future<AsyncConnectionPtr> NamedPipeClient::ConnectAsync(std::string pipeName)
{
    auto hPipe = CreatePipe(std::move(pipeName), true);
    if (hPipe == INVALID_HANDLE_VALUE)
        return std::future<AsyncConnectionPtr>();

    auto streamHandle = std::make_shared<boost::asio::windows::stream_handle>(*m_ioService, hPipe);

    auto connection = std::make_shared<AsyncConnection>(streamHandle);
    return connection->Connect();
}

HANDLE NamedPipeClient::CreatePipe(std::string pipeName, bool isAsync)
{
    pipeName = "\\\\.\\pipe\\" + pipeName;

    HANDLE hPipe = CreateFileA(
                pipeName.c_str(),
                GENERIC_READ | GENERIC_WRITE,
                0,
                nullptr,
                OPEN_EXISTING,
                isAsync ? FILE_FLAG_OVERLAPPED : 0,
                nullptr
                );

    return hPipe;
}
