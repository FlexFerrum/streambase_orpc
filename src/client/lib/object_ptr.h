#ifndef OBJECT_PTR_H
#define OBJECT_PTR_H

#include <common/types.h>
#include <common/param_value.h>

#include <future>
#include <memory>
#include <utility>

template<typename T>
struct ResultGetter
{
    static auto GetAsResult(const ParamValue& param)
    {
        return Result<T>(boost::get<T>(param));
    }
};

template<>
struct ResultGetter<void>
{
    static auto GetAsResult(const ParamValue&) {return Result<void>();}
};

class ISyncRpc
{
public:
    virtual ~ISyncRpc() {}

    virtual Result<ParamValue> InvokeSync(const std::string& methodName, ObjectHandle objId, ParamValueList params) = 0;
    virtual void ReleaseRemoteObject(ObjectHandle objId) = 0;
};

class IAsyncRpc
{
public:
    using AsyncCallback = std::function<void (Result<ParamValue>)>;

    virtual ~IAsyncRpc() {}

    virtual void InvokeAsync(const std::string& methodName, ObjectHandle objId, ParamValueList params, AsyncCallback callback) = 0;
    virtual void ReleaseRemoteObject(ObjectHandle objId) = 0;
};

class IObjectPtr
{
public:
    virtual ~IObjectPtr() {}

    virtual ObjectHandle GetHandle() = 0;
};

class SyncObjectPtr : public IObjectPtr
{
public:
    using InnerPtr = std::weak_ptr<ISyncRpc>;

    SyncObjectPtr() = default;
    SyncObjectPtr(ObjectHandle handle, InnerPtr ptr)
        : m_handle(handle)
        , m_ptr(ptr)
    {
    }

    ~SyncObjectPtr()
    {
        if (IsAlive())
            m_ptr.lock()->ReleaseRemoteObject(m_handle);
    }

    bool IsAlive() const
    {
        return !m_ptr.expired();
    }

    ObjectHandle GetHandle() override
    {
        return m_handle;
    }

    template<typename T>
    Result<T> GetAttribute(std::string name) const
    {
        if (!IsAlive())
            return nonstd::make_unexpected(eDisconnected);

        auto result = m_ptr.lock()->InvokeSync("GetAttribute", m_handle, {ParamValue{std::move(name)}});
        if (!result)
            return result.get_unexpected();

        return boost::get<T>(result.value());
    }


    template<typename R, typename ... Args>
    Result<R> CallMethod(std::string methodName, Args&& ... args) const
    {
        if (!IsAlive())
            return nonstd::make_unexpected(eDisconnected);

        auto result = CallMethodImpl(std::forward_as_tuple(std::move(methodName), std::forward<Args>(args)...),
                                                           std::make_index_sequence<sizeof ...(Args) + 1>());
        if (!result)
            return result.get_unexpected();

        return ResultGetter<R>::GetAsResult(result.value());
    }
private:
    template<typename Params, size_t ... Idx>
    Result<ParamValue> CallMethodImpl(Params&& params, std::index_sequence<Idx ...>) const
    {
        return m_ptr.lock()->InvokeSync("CallMethod", m_handle, {ParamValue{std::get<Idx>(params)}...});
    }

private:
    ObjectHandle m_handle = 0;
    InnerPtr m_ptr;
};

template<typename T>
using FutureResult = std::future<Result<T>>;

template<typename T>
using PromiseResult = std::promise<Result<T>>;

class AsyncObjectPtr : public IObjectPtr
{
public:
    using InnerPtr = std::weak_ptr<IAsyncRpc>;

    AsyncObjectPtr() = default;
    AsyncObjectPtr(ObjectHandle handle, InnerPtr ptr)
        : m_handle(handle)
        , m_ptr(ptr)
    {
    }

    ~AsyncObjectPtr()
    {
        if (IsAlive())
            m_ptr.lock()->ReleaseRemoteObject(m_handle);
    }

    bool IsAlive() const
    {
        return !m_ptr.expired();
    }

    ObjectHandle GetHandle() override
    {
        return m_handle;
    }

    template<typename T>
    FutureResult<T> GetAttribute(std::string name) const
    {
        if (!IsAlive())
            return FutureResult<T>();

        auto promise = std::make_shared<PromiseResult<T>>();
        m_ptr.lock()->InvokeAsync("GetAttribute", m_handle, {ParamValue{std::move(name)}}, [promise](const Result<ParamValue>& result) {
            if (!result)
                promise->set_value(result.get_unexpected());
            else
                promise->set_value(boost::get<T>(result.value()));
        });

        return promise->get_future();
    }

    template<typename R, typename ... Args>
    FutureResult<R> CallMethod(std::string methodName, Args&& ... args) const
    {
        if (!IsAlive())
            return std::future<Result<R>>();

        auto promise = std::make_shared<PromiseResult<R>>();

        CallMethodImpl(std::forward_as_tuple(
            std::move(methodName), std::forward<Args>(args)...),
            [promise](const Result<ParamValue>& result) {
                if (!result)
                    promise->set_value(result.get_unexpected());
                else
                    promise->set_value(ResultGetter<R>::GetAsResult(result.value()));
            },
            std::make_index_sequence<sizeof ...(Args) + 1>()
        );
        return promise->get_future();
    }
private:
    template<typename Params, typename Fn, size_t ... Idx>
    void CallMethodImpl(Params&& params, Fn&& callback, std::index_sequence<Idx ...>) const
    {
        m_ptr.lock()->InvokeAsync("CallMethod", m_handle, {ParamValue{std::get<Idx>(params)}...}, callback);
    }

private:
    ObjectHandle m_handle = 0;
    InnerPtr m_ptr;
};

#endif // SYNC_OBJECT_PTR_H
