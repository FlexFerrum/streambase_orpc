#include "async_connection.h"

#include <common/protocol.h>

#include <boost/asio/write.hpp>
#include <boost/asio/read.hpp>

AsyncConnection::AsyncConnection(std::shared_ptr<boost::asio::windows::stream_handle> serverHandle)
    : m_serverHandler(serverHandle)
{

}

AsyncConnection::~AsyncConnection()
{
}

std::future<AsyncConnectionPtr> AsyncConnection::Connect()
{
    messages::ConnectionRequest req;
    req.versionHi = 1;
    req.versionLo = 0;
    req.isAsync = true;

    m_tempConnectionHolder = shared_from_this();
    AsyncWrite(req, [this] {StartRead();});

    return m_connectionPromise.get_future();
}

FutureResult<AsyncObjectPtr> AsyncConnection::CreateRemoteObject(std::string typeName)
{
    messages::InvokeRequest req;
    req.methodName = "CreateObject";
    req.params.push_back(typeName);

    auto promise = std::make_shared<PromiseResult<AsyncObjectPtr>>();
    auto f = promise->get_future();

    {
        std::unique_lock<std::mutex> l(m_guardMutex);
        req.seqId = m_currentSeqId ++;
        m_asyncResults.insert(std::make_pair(req.seqId, [this, promise](Result<ParamValue> resultValue) {
            if (!resultValue)
            {
                promise->set_value(resultValue.get_unexpected());
            }
            else
            {
                auto& value = resultValue.value();
                auto handle = static_cast<ObjectHandle>(boost::get<int>(value));
                {
                    std::unique_lock<std::mutex> l(m_guardMutex);
                    m_liveObjects.insert(handle);
                }
                promise->set_value(AsyncObjectPtr(handle, shared_from_this()));
            }
        }));
    }
    AsyncWrite(req, [this] {});

    return f;
}

void AsyncConnection::InvokeAsync(const std::string& methodName, ObjectHandle objId, std::vector<ParamValue> params, IAsyncRpc::AsyncCallback callback)
{
    messages::InvokeRequest req;
    req.objectId = objId;
    req.methodName = methodName;
    req.params = std::move(params);

    {
        std::unique_lock<std::mutex> l(m_guardMutex);
        req.seqId = m_currentSeqId ++;
        m_asyncResults.insert(std::make_pair(req.seqId, std::move(callback)));
    }
    AsyncWrite(req, [this] {});
}

void AsyncConnection::ReleaseRemoteObject(ObjectHandle objId)
{
    // TODO:
}

void AsyncConnection::StartRead()
{
    boost::asio::async_read(*m_serverHandler, m_readBuffer.prepare(messages::MessageHeader::SerializedSize),
        [this](boost::system::error_code ec, std::size_t length)
        {
            if (!ec)
            {
                m_readBuffer.commit(length);
                std::istream is(&m_readBuffer);
                auto res = Deserialize<messages::MessageHeader>(is);
                m_readBuffer.consume(length);
                if (!res)
                    return;

                ProcessMessageHeader(res.value());
            }
        }
    );
}

void AsyncConnection::ProcessMessageHeader(const messages::MessageHeader& msgHeader)
{
    int messageType = msgHeader.type;

    void (AsyncConnection::*procFn)(std::istream&);

    switch (messageType)
    {
    case MT_ConnectResp:
        procFn = &AsyncConnection::ProcessConnectResponse;
        break;
    case MT_InvokeResp:
        procFn = &AsyncConnection::ProcessInvokeResponse;
        break;
    default:
        std::cerr << "[Client] Invalid incoming message type " << msgHeader.type << "\n";
        break;
    }

    if (msgHeader.length == 0)
    {
        std::istringstream is;
        (this->*procFn)(is);
        return;
    }


    boost::asio::async_read(*m_serverHandler, m_readBuffer.prepare(msgHeader.length),
        [this, procFn](boost::system::error_code ec, std::size_t length)
        {
            if (!ec)
            {
                m_readBuffer.commit(length);
                std::istream is(&m_readBuffer);
                (this->*procFn)(is);
                m_readBuffer.consume(length);
                StartRead();
            }
        }
    );
}

void AsyncConnection::ProcessConnectResponse(std::istream& is)
{
    std::shared_ptr<AsyncConnection> self;
    std::swap(self, m_tempConnectionHolder);

    auto res = Deserialize<messages::ConnectionResponse>(is);
    if (!res)
        return;

    m_connectionPromise.set_value(self);
    return;
}

void AsyncConnection::ProcessInvokeResponse(std::istream& is)
{
    auto res = Deserialize<messages::InvokeResponse>(is);
    if (!res)
        return;

    auto& resp = res.value();

    assert(m_asyncResults.count(resp.seqId) == 1);
    AsyncCallback callback;
    {
        std::unique_lock<std::mutex> l(m_guardMutex);
        callback = std::move(m_asyncResults[resp.seqId]);
        m_asyncResults.erase(resp.seqId);
    }
    if (resp.errorCode)
        callback(Result<ParamValue>(nonstd::make_unexpected(static_cast<ErrorCode>(resp.errorCode.get()))));
    else
        callback(Result<ParamValue>(resp.returnValue));

}
