#ifndef ASYNC_CONNECTION_H
#define ASYNC_CONNECTION_H

#include "object_ptr.h"

#include <common/protocol.h>

#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/read.hpp>

#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <future>
#include <mutex>

class AsyncConnection;
using AsyncConnectionPtr = std::shared_ptr<AsyncConnection>;

class AsyncConnection : public IAsyncRpc, public std::enable_shared_from_this<AsyncConnection>
{
public:
    AsyncConnection(std::shared_ptr<boost::asio::windows::stream_handle> serverHandle);
    ~AsyncConnection();

    std::future<AsyncConnectionPtr> Connect();
    std::future<Result<AsyncObjectPtr>> CreateRemoteObject(std::string typeName);

private:
    void InvokeAsync(const std::string& methodName, ObjectHandle objId, std::vector<ParamValue> params, AsyncCallback callback) override;
    void ReleaseRemoteObject(ObjectHandle objId) override;
    void StartRead();
    void ProcessMessageHeader(const messages::MessageHeader& msgHeader);
    void ProcessConnectResponse(std::istream& is);
    void ProcessInvokeResponse(std::istream& is);

    template<typename T, typename Fn>
    void AsyncWrite(T&& resp, Fn&& fn)
    {
        std::ostream os(&m_writeBuffer);
        PrepareMessage(resp, os);
        DoAsyncWrite(fn);
    }

    template<typename Fn>
    void DoAsyncWrite(Fn&& fn)
    {
        boost::system::error_code ec;
        auto length = boost::asio::write(*m_serverHandler, m_writeBuffer, ec);

        m_writeBuffer.consume(length);
        if (!ec)
        {
            fn();
        }
    }



private:
    std::shared_ptr<boost::asio::windows::stream_handle> m_serverHandler;

    std::promise<AsyncConnectionPtr> m_connectionPromise;
    std::shared_ptr<AsyncConnection> m_tempConnectionHolder;

    std::unordered_set<ObjectHandle> m_liveObjects;
    std::unordered_map<uint32_t, AsyncCallback> m_asyncResults;
    std::mutex m_guardMutex;

    boost::asio::streambuf m_readBuffer;
    boost::asio::streambuf m_writeBuffer;

    uint32_t m_currentSeqId = 1;
};

using AsyncConnectionPtr = std::shared_ptr<AsyncConnection>;

#endif // ASYNC_CONNECTION_H
