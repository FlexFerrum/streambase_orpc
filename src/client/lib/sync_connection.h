#ifndef SYNC_CONNECTION_H
#define SYNC_CONNECTION_H

#include "object_ptr.h"

#include <common/types.h>
#include <common/errors.h>
#include <common/protocol.h>

#include <typeinfo>

#include <unordered_set>

class SyncConnection : public std::enable_shared_from_this<SyncConnection>, public ISyncRpc
{
public:
    SyncConnection(HANDLE serverHandler);
    ~SyncConnection();

    Result<void> Connect();

    Result<SyncObjectPtr> CreateRemoteObject(std::string typeName);
    void ReleaseRemoteObject(ObjectHandle objId);

    // ISyncRpc interface
    Result<ParamValue> InvokeSync(const std::string& methodName, ObjectHandle objId, std::vector<ParamValue> params) override;

private:
    HANDLE m_serverHandler;

    std::unordered_set<ObjectHandle> m_liveObjects;
};

#endif // SYNC_CONNECTION_H
