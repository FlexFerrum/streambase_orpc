#include "sync_connection.h"

#include <common/protocol.h>
#include <common/errors.h>

#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>

#include <iostream>

template<typename T>
Result<void> WriteToServer(HANDLE hPipe, T&& msg)
{
    auto os = MakeWStream();
    auto sizeToWrite = PrepareMessage(std::forward<T>(msg), os);

    auto buff = os.str();
    DWORD writtenMsg = 0;
    BOOL writeResult = WriteFile(hPipe, buff.data(), static_cast<DWORD>(sizeToWrite), &writtenMsg, nullptr);
    if (!writeResult)
        return nonstd::make_unexpected(eUnexpected);

    if (sizeToWrite != static_cast<size_t>(writtenMsg))
        return nonstd::make_unexpected(eUnexpected);

    return Result<void>();
}

bool ReadFromPipe(HANDLE hPipe, size_t expectedSize, std::ostream& os)
{
    const DWORD tempBuffSize = 0x4000;
    char buff[tempBuffSize];

    DWORD bytesToRead = static_cast<DWORD>(expectedSize);

    while (bytesToRead != 0)
    {
        DWORD readBytes = 0;
        if (!ReadFile(hPipe, buff, std::min(bytesToRead, tempBuffSize), &readBytes, nullptr))
            return false;

        os.write(buff, readBytes);
        bytesToRead -= readBytes;
    }

    return true;
}

template<typename T>
Result<T> ReadFromServer(HANDLE hPipe)
{
    auto msgHdrOs = MakeRWStream();
    auto msgBodyOs = MakeRWStream();

    if (!ReadFromPipe(hPipe, messages::MessageHeader::SerializedSize, msgHdrOs))
        return nonstd::make_unexpected(eUnexpected);

    messages::MessageHeader msgHeader;
    {
        auto result = Deserialize<messages::MessageHeader>(msgHdrOs);
        if (!result)
            return result.get_unexpected();
        msgHeader = result.value();
    }

    if (!ReadFromPipe(hPipe, msgHeader.length, msgBodyOs))
        return nonstd::make_unexpected(eUnexpected);

    return Deserialize<T>(msgBodyOs);
}

SyncConnection::SyncConnection(HANDLE serverHandler)
    : m_serverHandler(serverHandler)
{

}

SyncConnection::~SyncConnection()
{
    CloseHandle(m_serverHandler);
}

Result<void> SyncConnection::Connect()
{
    messages::ConnectionRequest req;
    req.versionHi = 1;
    req.versionLo = 0;
    req.isAsync = false;

    if (!WriteToServer(m_serverHandler, req))
        return nonstd::make_unexpected(eUnexpected);

    auto serverResponse = ReadFromServer<messages::ConnectionResponse>(m_serverHandler);

    if (!serverResponse)
        return nonstd::make_unexpected(eUnexpected);

    return Result<void>();
}

Result<SyncObjectPtr> SyncConnection::CreateRemoteObject(std::string typeName)
{
    messages::InvokeRequest req;
    req.methodName = "CreateObject";
    req.params.push_back(typeName);

    if (!WriteToServer(m_serverHandler, req))
    {
        std::cerr << "Can't send method invocateion message to server side.\n";
        return nonstd::make_unexpected(eUnexpected);
    }

    auto serverResponse = ReadFromServer<messages::InvokeResponse>(m_serverHandler);

    if (!serverResponse)
    {
        std::cerr << "Can't read invocation message response from server side.\n";
        return nonstd::make_unexpected(eUnexpected);
    }

    const messages::InvokeResponse& resp = serverResponse.value();

    if (resp.errorCode.is_initialized())
        return nonstd::make_unexpected(static_cast<ErrorCode>(resp.errorCode.get()));

    auto handle = *boost::get<int>(&resp.returnValue);
    m_liveObjects.insert(handle);

    return SyncObjectPtr(handle, shared_from_this());
}

void SyncConnection::ReleaseRemoteObject(ObjectHandle objId)
{
    // TODO:
}

Result<ParamValue> SyncConnection::InvokeSync(const std::string& methodName, ObjectHandle objId, std::vector<ParamValue> params)
{
    messages::InvokeRequest req;
    req.objectId = objId;
    req.methodName = methodName;
    req.params = std::move(params);
    req.seqId = 0xffffffff;

    if (!WriteToServer(m_serverHandler, req))
    {
        std::cerr << "Can't send method invocateion message to server side.\n";
        return nonstd::make_unexpected(eUnexpected);
    }

    auto serverResponse = ReadFromServer<messages::InvokeResponse>(m_serverHandler);

    if (!serverResponse)
    {
        std::cerr << "Can't read invocation message response from server side.\n";
        return nonstd::make_unexpected(eUnexpected);
    }

    const messages::InvokeResponse& resp = serverResponse.value();

    if (resp.errorCode.is_initialized())
        return nonstd::make_unexpected(static_cast<ErrorCode>(resp.errorCode.get()));

    return std::move(resp.returnValue);
}
