#include "common/protocol.h"
#include "common/errors.h"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

#include <sstream>

#include <WinSock2.h>

template<typename T>
void SerializeImpl(const T& data, std::ostream& os)
{
    boost::archive::binary_oarchive arch(os);
    arch << data;
}

template<typename T>
T DeserializeImpl(std::istream& is)
{
    boost::archive::binary_iarchive arch(is);

    T result;
    arch >> result;

    return result;
}

namespace messages
{

void MessageHeader::Serialize(const MessageHeader& msgHdr, std::ostream& os)
{
    auto write = [&os](auto data) {
        const char* buff = reinterpret_cast<const char*>(&data);
        auto dataSize = sizeof(data);
        os.write(buff, dataSize);
    };

    write(static_cast<uint8_t>(msgHdr.type));
    write(htonl(msgHdr.length));
}

Result<MessageHeader> MessageHeader::Deserialize(std::istream& is)
{
    using result_t = nonstd::expected<void, ErrorCode>;

    auto read = [&is](auto& data) -> result_t {
        auto dataSize = sizeof(data);
        is.read(reinterpret_cast<char*>(&data), dataSize);
        if (static_cast<size_t>(is.gcount()) < dataSize)
            return nonstd::make_unexpected(eDataTooSmall);

        return result_t();
    };

    uint8_t msgType = 0;
    result_t result = read(msgType);
    if (!result)
        return result.get_unexpected();

    u_long msgLen = 0;
    result = read(msgLen);
    if (!result)
        return result.get_unexpected();

    return MessageHeader{static_cast<MessageType>(msgType), ntohl(msgLen)};
}

void ConnectionRequest::Serialize(const ConnectionRequest& req, std::ostream& os)
{
    SerializeImpl(req, os);
}

Result<ConnectionRequest> ConnectionRequest::Deserialize(std::istream& is)
{
    return DeserializeImpl<ConnectionRequest>(is);
}

void ConnectionResponse::Serialize(const ConnectionResponse& resp, std::ostream& os)
{
    SerializeImpl(resp, os);
}

Result<ConnectionResponse> ConnectionResponse::Deserialize(std::istream& is)
{
    return DeserializeImpl<ConnectionResponse>(is);
}

void InvokeRequest::Serialize(const InvokeRequest& req, std::ostream& os)
{
    SerializeImpl(req, os);
}

Result<InvokeRequest> InvokeRequest::Deserialize(std::istream& is)
{
    return DeserializeImpl<InvokeRequest>(is);
}

void InvokeResponse::Serialize(const InvokeResponse& resp, std::ostream& os)
{
    SerializeImpl(resp, os);
}

Result<InvokeResponse> InvokeResponse::Deserialize(std::istream& is)
{
    return DeserializeImpl<InvokeResponse>(is);
}

} // messages
