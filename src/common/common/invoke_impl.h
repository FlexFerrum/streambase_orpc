#ifndef INVOKE_IMPL_H
#define INVOKE_IMPL_H

#include <common/param_value.h>

#include <vector>
#include <type_traits>
#include <utility>

namespace detail
{
template<typename T, typename R, typename FnPtr>
struct FunctionCaller : public boost::static_visitor<R>
{
    T* object;
    FnPtr function;

    struct NoType
    {
    };

    template<typename ... Args>
    struct CanBeCalled
    {
        template<typename U, typename F>
        static auto TestFn(U* obj, F&& f) -> decltype((obj->*f)(std::declval<Args>()...));
        static auto TestFn(...) -> NoType;

        using result_type = decltype(TestFn((T*)nullptr, std::declval<FnPtr>()));
        enum {value = !std::is_same<result_type, NoType>::value};
    };

    FunctionCaller(T* obj, FnPtr fn)
        : object(obj)
        , function(fn)
    {}

    template<typename ... Args>
    auto operator()(Args&& ... args) const -> std::enable_if_t<CanBeCalled<Args...>::value, R>
    {
        return (object->*function)(std::forward<Args>(args)...);
    }

    template<typename ... Args>
    auto operator()(Args&& ... args) const -> std::enable_if_t<!CanBeCalled<Args...>::value, R>
    {
        return R();
    }

};

template<typename T, typename ... Args, size_t ... Idxs>
void InvokeImpl(T* obj, void (T::*fnPtr)(Args...), const std::vector<ParamValue>& params, const std::index_sequence<Idxs...>&)
{
    boost::apply_visitor(FunctionCaller<T, void, void (T::*)(Args ...)>(obj, fnPtr), params[Idxs]...);
}

template<typename R, typename T, typename ... Args, size_t ... Idxs>
R InvokeImpl(T* obj, R (T::*fnPtr)(Args...), const std::vector<ParamValue>& params, const std::index_sequence<Idxs...>&)
{
    return boost::apply_visitor(FunctionCaller<T, R, R (T::*)(Args ...)>(obj, fnPtr), params[Idxs]...);
}
} // detail

template<typename R, typename T1, typename T2, typename ... Args>
R Invoke(T1* obj, R (T2::*fnPtr)(Args...), const std::vector<ParamValue>& params)
{
    return detail::InvokeImpl(static_cast<T2*>(obj), fnPtr, params, std::make_index_sequence<sizeof ... (Args)>());
};


#endif // INVOKE_IMPL_H
