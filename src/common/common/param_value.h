#ifndef COMMON_PARAM_VALUE_H
#define COMMON_PARAM_VALUE_H

#include <boost/variant.hpp>

#include <vector>

struct VoidType
{
    template<class Ar>
    void serialize(Ar& ar, const unsigned int /*ver*/)
    {
    }
};

using ParamValue = boost::variant<VoidType, bool, int, double, std::string>;
using ParamValueList = std::vector<ParamValue>;

#endif // COMMON_PARAM_VALUE_H
