#ifndef COMMON_ERRORS_H
#define COMMON_ERRORS_H

#include <nonstd/expected.hpp>

enum ErrorCode
{
    eUnexpected = 1,
    eDataTooSmall,
    eTypeNotFound,
    eMethodNotFound,
    eAttributeNotFound,
    eObjectNotFound,
    eDisconnected
};

template<typename T>
using Result = nonstd::expected<T, ErrorCode>;

#endif // COMMON_ERRORS_H
