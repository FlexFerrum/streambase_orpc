#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

#include <boost/asio/io_service.hpp>
#include <boost/asio/windows/stream_handle.hpp>

#include "errors.h"

using IoService = boost::asio::io_service;
using IoServicePtr = std::shared_ptr<IoService>;
using StreamHandler = boost::asio::windows::stream_handle;
using StreamHandlerPtr = std::shared_ptr<StreamHandler>;
using DefaultEventHandler = std::function<void ()>;
using ObjectHandle = uint32_t;


#endif // COMMON_TYPES_H
