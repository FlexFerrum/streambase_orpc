#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "errors.h"
#include "param_value.h"

#include <boost/optional.hpp>
#include <boost/serialization/split_member.hpp>

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

enum MessageType
{
    MT_ConnectReq,
    MT_ConnectResp,
    MT_InvokeReq,
    MT_InvokeResp,
    MT_DisconnectReq
};

namespace messages
{

struct MessageHeader
{
    MessageType type;
    uint32_t length;

    template<class Ar>
    void serialize(Ar& ar, const unsigned int /*ver*/)
    {
        ar & static_cast<int>(type);
        ar & length;
    }

    static void Serialize(const MessageHeader& msgHdr, std::ostream& os);
    static Result<MessageHeader> Deserialize(std::istream& is);

    enum : size_t
    {
        SerializedSize = 5,
    };
};

struct ConnectionRequest
{

    enum : uint8_t
    {
        MessageTypeId = MT_ConnectReq
    };

    uint8_t versionHi;
    uint8_t versionLo;
    bool isAsync;

    template<class Ar>
    void serialize(Ar& ar, const unsigned int /*ver*/)
    {
        ar & versionHi;
        ar & versionLo;
        ar & isAsync;
    }

    static void Serialize(const ConnectionRequest& req, std::ostream& os);
    static Result<ConnectionRequest> Deserialize(std::istream& is);
};

struct ConnectionResponse
{
    enum : uint8_t
    {
        MessageTypeId = MT_ConnectResp
    };

    uint8_t versionHi;
    uint8_t versionLo;

    template<class Ar>
    void serialize(Ar& ar, const unsigned int /*ver*/)
    {
        ar & versionHi;
        ar & versionLo;
    }

    static void Serialize(const ConnectionResponse& resp, std::ostream& os);
    static Result<ConnectionResponse> Deserialize(std::istream& is);
};

struct InvokeRequest
{
    enum : uint8_t
    {
        MessageTypeId = MT_InvokeReq
    };

    uint32_t seqId;
    boost::optional<uint32_t> objectId;
    std::string methodName;
    std::vector<ParamValue> params;

    template<class Ar>
    void save(Ar& ar, const unsigned int /*ver*/) const
    {
        ar << seqId;
        bool hasObjectId = objectId.is_initialized();
        ar << hasObjectId;
        if (hasObjectId)
            ar << objectId.get();

        ar << methodName;
        ar << params;
    }

    template<class Ar>
    void load(Ar& ar, const unsigned int /*ver*/)
    {
        ar >> seqId;
        bool hasObjectId = false;
        ar >> hasObjectId;
        if (hasObjectId)
        {
            uint32_t id;
            ar >> id;
            objectId = id;
        }

        ar >> methodName;
        ar >> params;
    }

    static void Serialize(const InvokeRequest& resp, std::ostream& os);
    static Result<InvokeRequest> Deserialize(std::istream& is);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

struct InvokeResponse
{
    enum : uint8_t
    {
        MessageTypeId = MT_InvokeResp
    };

    uint32_t seqId;
    boost::optional<uint32_t> errorCode;
    ParamValue returnValue;

    template<class Ar>
    void save(Ar& ar, const unsigned int /*ver*/) const
    {
        ar << seqId;
        bool hasError = errorCode.is_initialized();
        ar << hasError;
        if (hasError)
            ar << errorCode.get();

        ar << returnValue;
    }

    template<class Ar>
    void load(Ar& ar, const unsigned int /*ver*/)
    {
        ar >> seqId;
        bool hasError = false;
        ar >> hasError;
        if (hasError)
        {
            uint32_t code;
            ar >> code;
            errorCode = code;
        }

        ar >> returnValue;
    }

    static void Serialize(const InvokeResponse& resp, std::ostream& os);
    static Result<InvokeResponse> Deserialize(std::istream& is);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} // messages

template<typename T>
auto Serialize(T&& arg, std::ostream& os)
{
    return std::decay_t<T>::Serialize(arg, os);
}

template<typename T>
auto Deserialize(std::istream& is)
{
    return T::Deserialize(is);
}


inline std::ostringstream MakeWStream()
{
    return std::ostringstream(std::ios_base::out | std::ios_base::binary);
}

inline std::stringstream MakeRWStream()
{
    return std::stringstream(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
}

inline std::istringstream MakeRStream(const std::string& data)
{
    return std::istringstream(data, std::ios_base::in | std::ios_base::binary);
}

template<typename T>
size_t PrepareMessage(T&& arg, std::ostream& outBuff)
{
    auto msgStream = MakeWStream();
    Serialize(std::forward<T>(arg), msgStream);
    auto msgData = msgStream.str();

    messages::MessageHeader msgHdr;
    msgHdr.type = static_cast<MessageType>(std::decay_t<T>::MessageTypeId);
    msgHdr.length = static_cast<uint32_t>(msgData.size());

    Serialize(msgHdr, outBuff);
    outBuff.write(msgData.data(), msgData.size());

    return msgData.size() + messages::MessageHeader::SerializedSize;
}

#endif // PROTOCOL_H
