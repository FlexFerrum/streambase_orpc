#include "named_pipe_server.h"
#include "remote_object_manager.h"

#include <iostream>
#include <string>

class ServerObject
{
public:
    std::string Echo(const std::string& str)
    {
        return str;
    }

    void WriteToConsole(const std::string& str)
    {
        switch (m_writeMode)
        {
        case 0:
            std::cout << "From client: ";
            break;
        case 1:
            std::cout << "FROM CLIENT: ";
            break;
        case 2:
            std::cout << "[CLIENT]: ";
            break;
        }

        std::cout << str << std::endl;
    }

    void SetWriteMode(int writeMode)
    {
        m_writeMode = writeMode;
    }
    int GetWriteMode() const {return m_writeMode;}
    std::string GetObjectName() const {return m_objectName;}

private:
    std::string m_objectName = "ServerObject";
    int m_writeMode = 0;
};

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: orpc_server <pipe_name>\n";
        return -1;
    }

    RemoteObjectManager objectManager;

    objectManager.RegisterType<ServerObject>(
        "ServerObject",
        {
            {"consoleWriteMode", reflect::Attribute(&ServerObject::GetWriteMode)},
            {"objectName", reflect::Attribute(&ServerObject::GetObjectName)},
        },
        {
            {"Echo", reflect::Method(&ServerObject::Echo)},
            {"WriteToConsole", reflect::Method(&ServerObject::WriteToConsole)},
            {"SetConsoleWriteMode", reflect::Method(&ServerObject::SetWriteMode)},
        }
    );

    NamedPipeServer server(argv[1], &objectManager);

    server.Start();
    std::cout << "Started listening on '" << argv[1] << "' Press enter to exit...\n";
    std::cin.get();
    server.Stop();
}
