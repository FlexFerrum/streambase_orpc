#include "connection_processor.h"
#include "named_pipe_server.h"
#include "remote_object_manager.h"
#include <common/invoke_impl.h>

#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/optional/optional_io.hpp>

#include <iostream>

ConnectionProcessor::ConnectionProcessor(StreamHandlerPtr clientHandler, NamedPipeServer* server)
    : m_clientHandler(clientHandler)
    , m_server(server)
{

}

void ConnectionProcessor::Start()
{
    StartRead();
}

void ConnectionProcessor::StartRead()
{
    auto self(shared_from_this());

    boost::asio::async_read(*m_clientHandler, m_readBuffer.prepare(messages::MessageHeader::SerializedSize),
        [this, self](boost::system::error_code ec, std::size_t length)
        {
            if (!ec)
            {
                m_readBuffer.commit(length);
                std::istream is(&m_readBuffer);
                auto res = Deserialize<messages::MessageHeader>(is);
                m_readBuffer.consume(length);
                if (!res)
                    return;

                ProcessMessageHeader(res.value());
            }
        }
    );
}

void ConnectionProcessor::DoAsyncWrite()
{
    boost::system::error_code ec;
    auto length = boost::asio::write(*m_clientHandler, m_writeBuffer, ec);
    m_writeBuffer.consume(length);
}

void ConnectionProcessor::ProcessMessageHeader(const messages::MessageHeader& msgHeader)
{
    if (m_state != State::Connected && msgHeader.type != MT_ConnectReq)
    {
        return;
    }
    else if (m_state == State::Connected && msgHeader.type == MT_ConnectReq)
    {
        return;
    }

    int messageType = msgHeader.type;

    bool (ConnectionProcessor::*procFn)(std::istream&);
    auto self = shared_from_this();

    switch (messageType)
    {
    case MT_ConnectReq:
        procFn = &ConnectionProcessor::Connect;
        break;
    case MT_InvokeReq:
        procFn = &ConnectionProcessor::Invoke;
        break;
    case MT_DisconnectReq:
        procFn = &ConnectionProcessor::Disconnect;
        break;
    default:
        std::cerr << "[Client] Invalid incoming message type " << msgHeader.type << "\n";
        break;
    }

    if (msgHeader.length == 0)
    {
        std::istringstream is;
        if ((this->*procFn)(is))
        {
            StartRead();
            return;
        }
        return;
    }


    boost::asio::async_read(*m_clientHandler, m_readBuffer.prepare(msgHeader.length),
        [this, self, procFn](boost::system::error_code ec, std::size_t length)
        {
            if (!ec)
            {
                m_readBuffer.commit(length);
                std::istream is(&m_readBuffer);
                if ((this->*procFn)(is))
                {
                    m_readBuffer.consume(length);
                    StartRead();
                    return;
                }
            }
        }
    );
}

bool ConnectionProcessor::Connect(std::istream& is)
{
    auto res = Deserialize<messages::ConnectionRequest>(is);
    if (!res)
        return false;

    m_state = State::Connected;

    messages::ConnectionResponse resp{1, 0};
    AsyncWriteResponse(resp);
    return true;
}

bool ConnectionProcessor::Invoke(std::istream& is)
{
    auto res = Deserialize<messages::InvokeRequest>(is);
    if (!res)
        return false;

    auto req = res.value();

    if (m_isAsyncConnection)
        m_clientHandler->get_io_service().post([req, this]{InvokeObjectMethod(req);});

    return InvokeObjectMethod(req);
}

bool ConnectionProcessor::InvokeObjectMethod(const messages::InvokeRequest& req)
{
    if (!req.objectId)
        return InvokeSpecialMethod(req.seqId, req.methodName, req.params);

    messages::InvokeResponse resp;
    resp.seqId = req.seqId;

    auto p = m_objects.find(req.objectId.get());
    if (p == m_objects.end())
    {
        resp.errorCode = eObjectNotFound;
        AsyncWriteResponse(resp);
        return true;
    }

    Result<ParamValue> callResult;

    if (req.methodName == "CallMethod")
    {
        auto objMethodName = boost::get<std::string>(req.params[0]);
        ParamValueList params(req.params.begin() + 1, req.params.end());
        callResult = p->second.stub.CallMethod(objMethodName, params);
    }
    else if (req.methodName == "GetAttribute")
    {
        auto objAttrName = boost::get<std::string>(req.params[0]);
        callResult = p->second.stub.GetAttribute(objAttrName);
    }
    else
        callResult = nonstd::make_unexpected(eMethodNotFound);

    if (!callResult)
        resp.errorCode = callResult.error();
    else
        resp.returnValue = callResult.value();

    AsyncWriteResponse(resp);
    return true;
}

bool ConnectionProcessor::InvokeSpecialMethod(uint32_t seqId, const std::string& methodName, const std::vector<ParamValue>& params)
{
    if (methodName == "CreateObject")
    {
        RemoteObjectManager* objectManager = m_server->GetRemoteObjectManager();
        auto callResult = ::Invoke(objectManager, &RemoteObjectManager::CreateObject, params);

        messages::InvokeResponse resp;

        if (!callResult)
        {
            resp.seqId = seqId;
            resp.errorCode = callResult.error();
        }
        else
        {
            ObjectInfo objInfo{m_curObjectId, callResult.value()};
            m_objects[objInfo.objectId] = std::move(objInfo);

            resp.seqId = seqId;
            resp.returnValue = static_cast<int>(objInfo.objectId);
        }

        AsyncWriteResponse(resp);
        return true;
    }

    return false;
}

bool ConnectionProcessor::Disconnect(std::istream& is)
{
    return false;
}

