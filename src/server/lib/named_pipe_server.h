#ifndef NAMED_PIPE_SERVER_H
#define NAMED_PIPE_SERVER_H

#include <common/types.h>

#include <boost/asio/io_service.hpp>
#include <boost/asio/windows/stream_handle.hpp>
#include <boost/system/error_code.hpp>

#include <string>
#include <functional>
#include <thread>

class RemoteObjectManager;

class NamedPipeServer
{
public:
    NamedPipeServer(std::string pipeName, RemoteObjectManager* objManager);

    void Start();
    void Stop();

    void SetOnServerStarted(std::function<void ()> fn)
    {
        m_onServerStarted = std::move(fn);
    }

    void SetOnServerStopped(std::function<void ()> fn)
    {
        m_onServerStopped = std::move(fn);
    }

    void SetOnPipeCreated(std::function<void (bool isOk)> fn)
    {
        m_onPipeCreated = std::move(fn);
    }

    void SetOnClientAccepted(std::function<void ()> fn)
    {
        m_onClientAccepted = std::move(fn);
    }

    auto GetRemoteObjectManager() const {return m_remoteObjectManager;}

private:
    void AcceptClientConnection();
    void OnClientConnected(StreamHandlerPtr handler, const boost::system::error_code& ec, size_t bytesTransferred);

private:
    std::string m_pipeName;
    std::shared_ptr<boost::asio::io_service> m_ioService;
    std::shared_ptr<boost::asio::io_service::work>  m_work;
    std::vector<std::thread> m_serviceThreads;

    DefaultEventHandler m_onServerStarted;
    DefaultEventHandler m_onServerStopped;
    DefaultEventHandler m_onClientAccepted;
    std::function<void (bool)> m_onPipeCreated;
    RemoteObjectManager* m_remoteObjectManager;
};

#endif // NAMED_PIPE_SERVER_H
