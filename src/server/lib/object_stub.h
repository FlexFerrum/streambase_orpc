#ifndef OBJECT_STUB_H
#define OBJECT_STUB_H

#include <common/errors.h>
#include <common/param_value.h>

#include <functional>
#include <string>
#include <memory>
#include <unordered_map>

struct TypeInfo
{
    using MethodAccessor = std::function<ParamValue (void* objPtr, const ParamValueList& params)>;
    using AttrAccessor = std::function<ParamValue (void* objPtr)>;

    std::string typeName;
    std::unordered_map<std::string, MethodAccessor> methods;
    std::unordered_map<std::string, AttrAccessor> attributes;
    std::function<std::shared_ptr<void>()> constructor;
};

class ObjectStub
{
public:
    ObjectStub() = default;

    ObjectStub(std::shared_ptr<void> object, const TypeInfo* typeInfo)
        : m_object(object)
        , m_typeInfo(typeInfo)
    {
    }

    Result<ParamValue> CallMethod(const std::string& methodName, const ParamValueList& params)
    {
        auto p = m_typeInfo->methods.find(methodName);
        if (p == m_typeInfo->methods.end())
            return nonstd::make_unexpected(eMethodNotFound);

        return p->second(m_object.get(), params);
    }

    Result<ParamValue> GetAttribute(const std::string& attrName)
    {
        auto p = m_typeInfo->attributes.find(attrName);
        if (p == m_typeInfo->attributes.end())
            return nonstd::make_unexpected(eAttributeNotFound);

        return p->second(m_object.get());
    }

private:
    std::shared_ptr<void> m_object;
    const TypeInfo* m_typeInfo = nullptr;
};

#endif // OBJECT_STUB_H
