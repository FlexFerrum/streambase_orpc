#ifndef CONNECTION_PROCESSOR_H
#define CONNECTION_PROCESSOR_H

#include "object_stub.h"

#include <common/protocol.h>
#include <common/types.h>
#include <common/param_value.h>

#include <boost/asio/streambuf.hpp>
#include <boost/asio/buffered_read_stream.hpp>

#include <memory>
#include <mutex>
#include <unordered_map>

class NamedPipeServer;
class ObjectStub;

class ConnectionProcessor : public std::enable_shared_from_this<ConnectionProcessor>
{
public:
    ConnectionProcessor(StreamHandlerPtr clientHandler, NamedPipeServer* server);

    void Start();

private:
    enum class State
    {
        NotConnected,
        Connected,
        Disconnected
    };

    void StartRead();
    void ProcessMessageHeader(const messages::MessageHeader& msgHeader);
    bool Connect(std::istream& is);
    bool Invoke(std::istream& is);
    bool InvokeObjectMethod(const messages::InvokeRequest& req);
    bool InvokeSpecialMethod(uint32_t seqId, const std::string& methodName, const std::vector<ParamValue>& params);
    bool Disconnect(std::istream& is);

    template<typename T>
    void AsyncWriteResponse(T&& resp)
    {
        std::ostream os(&m_writeBuffer);
        size_t totalSize = PrepareMessage(resp, os);
        DoAsyncWrite();
    }

    void DoAsyncWrite();

private:
    struct ObjectInfo
    {
        ObjectHandle objectId;
        ObjectStub stub;
    };

    StreamHandlerPtr m_clientHandler;
    State m_state = State::NotConnected;
    NamedPipeServer* m_server = nullptr;
    bool m_isAsyncConnection = false;

    boost::asio::streambuf m_readBuffer;
    boost::asio::streambuf m_writeBuffer;

    std::atomic<ObjectHandle> m_curObjectId = 1;
    std::unordered_map<ObjectHandle, ObjectInfo> m_objects;
};

#endif // CONNECTION_PROCESSOR_H
