#include "connection_processor.h"
#include "named_pipe_server.h"

#include <boost/asio/windows/overlapped_ptr.hpp>

#include <iostream>

NamedPipeServer::NamedPipeServer(std::string pipeName, RemoteObjectManager* objManager)
    : m_pipeName(std::move(pipeName))
    , m_remoteObjectManager(objManager)
{

}

void NamedPipeServer::Start()
{
    m_ioService = std::make_shared<boost::asio::io_service>();
    m_work = std::make_shared<boost::asio::io_service::work>(*m_ioService);
    for (int n = 0; n < 4; ++ n)
    {
        m_serviceThreads.push_back(std::thread([this]
        {
            m_ioService->run();
            if (m_onServerStopped)
                m_onServerStopped();
        }));
    }

    m_ioService->post([this]
    {
        if (m_onServerStarted)
            m_onServerStarted();

        AcceptClientConnection();
    });
}

void NamedPipeServer::Stop()
{
    if (!m_ioService)
        return;

    m_ioService->stop();
    m_work.reset();
    for (auto& th : m_serviceThreads)
        th.join();

    m_serviceThreads.clear();
    m_ioService.reset();
}

void NamedPipeServer::AcceptClientConnection()
{
    auto pipeName = "\\\\.\\pipe\\" + m_pipeName;
    HANDLE hPipe = CreateNamedPipeA(
                pipeName.c_str(),
                PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
                PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS,
                PIPE_UNLIMITED_INSTANCES,
                0x4000,
                0x4000,
                0,
                nullptr);

    if (hPipe == INVALID_HANDLE_VALUE)
    {
        DWORD err = GetLastError();
        if (m_onPipeCreated)
            m_onPipeCreated(false);

        return;
    }

    auto streamHandle = std::make_shared<boost::asio::windows::stream_handle>(*m_ioService, hPipe);

    boost::asio::windows::overlapped_ptr overlappedPtr(*m_ioService, [this, streamHandle](const boost::system::error_code& ec, size_t bytesTransferred) {
        OnClientConnected(streamHandle, ec, bytesTransferred);
    });

    OVERLAPPED* overlapped = overlappedPtr.get();
    BOOL ok = ConnectNamedPipe(hPipe, overlapped);
    DWORD lastError = GetLastError();

    if (!ok && lastError != ERROR_IO_PENDING)
    {
        boost::system::error_code ec(lastError,
                                     boost::asio::error::get_system_category());
        if (m_onPipeCreated)
            m_onPipeCreated(false);

        overlappedPtr.complete(ec, 0);
    }
    else
    {
        overlappedPtr.release();
        if (m_onPipeCreated)
            m_onPipeCreated(true);
    }
}

void NamedPipeServer::OnClientConnected(StreamHandlerPtr handler, const boost::system::error_code& ec, size_t bytesTransferred)
{
    if (m_onClientAccepted)
        m_onClientAccepted();

    if (!ec)
    {
        auto conn = std::make_shared<ConnectionProcessor>(handler, this);
        conn->Start();
    }
    m_ioService->post([this]{AcceptClientConnection();});
}

