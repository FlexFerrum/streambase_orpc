#ifndef REMOTE_OBJECT_MANAGER_H
#define REMOTE_OBJECT_MANAGER_H

#include "object_stub.h"

#include <common/invoke_impl.h>

#include <initializer_list>
#include <memory>
#include <string>
#include <unordered_map>

namespace reflect
{
template<typename V, typename T>
TypeInfo::AttrAccessor Attribute(V T::*ptr)
{
    return [ptr](void* obj) -> ParamValue {
        return reinterpret_cast<T*>(obj)->*ptr;
    };
}

template<typename V, typename T>
TypeInfo::AttrAccessor Attribute(V (T::*ptr)()const)
{
    return [ptr](void* obj) -> ParamValue {
        return (reinterpret_cast<T*>(obj)->*ptr)();
    };
}

template<typename R, typename T, typename ... Args>
TypeInfo::MethodAccessor Method(R (T::*ptr)(Args...))
{
    return [ptr](void* obj, const ParamValueList& params) -> ParamValue {
        return Invoke(reinterpret_cast<T*>(obj), ptr, params);
    };
}

template<typename T, typename ... Args>
TypeInfo::MethodAccessor Method(void (T::*ptr)(Args...))
{
    return [ptr](void* obj, const ParamValueList& params) -> ParamValue {
        Invoke(reinterpret_cast<T*>(obj), ptr, params);
        return ParamValue(VoidType{});
    };
}}

class RemoteObjectManager
{
public:
    template<typename T>
    void RegisterType(std::string typeName,
                      const std::initializer_list<std::pair<std::string, TypeInfo::AttrAccessor>>& attributes,
                      const std::initializer_list<std::pair<std::string, TypeInfo::MethodAccessor>>& methods)
    {
        TypeInfo info;
        info.typeName = typeName;
        info.methods = std::unordered_map<std::string, TypeInfo::MethodAccessor>(methods.begin(), methods.end());
        info.attributes = std::unordered_map<std::string, TypeInfo::AttrAccessor>(attributes.begin(), attributes.end());
        info.constructor = []
        {
            auto ptr = std::make_shared<T>();

            return std::shared_ptr<void>(ptr, ptr.get());
        };

        m_registeredTypes.insert(std::make_pair(std::move(typeName), std::move(info)));
    }

    Result<ObjectStub> CreateObject(const std::string& typeName)
    {
        auto p = m_registeredTypes.find(typeName);
        if (p == m_registeredTypes.end())
            return nonstd::make_unexpected(eTypeNotFound);

        const TypeInfo* ti = &p->second;
        return ObjectStub(ti->constructor(), ti);
    }

private:
    std::unordered_map<std::string, TypeInfo> m_registeredTypes;
};

#endif // REMOTE_OBJECT_MANAGER_H
